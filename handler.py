import os
import json
import pandas as pd
import urllib.parse
import boto3
import requests
import logging
import io

# logging
logger = logging.getLogger('uploader')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

s3 = boto3.client('s3')
ssm = boto3.client('ssm')


required_headers = ["title", "text", "author", "source", "rid", "sku", "rating", "date", "ispublished", "verified", "productname", "sourcecategory", "desc"]

auth_token_url = None
csv_upload_url = None
username_param = None
password_param = None


try:
    auth_token_url = os.environ['token_req_endpoint']
except:
    auth_token_url = "https://devapi.bewgle.com/api/auth/token"

try:
    csv_upload_url = os.environ['csv_upload_endpoint']
except:
    csv_upload_url = "https://devapi.bewgle.com/api/reviews/listings/upload"

try:
    username_param = os.environ['username_param']
except:
    username_param = '/CsvUploader/username'

try:
    password_param = os.environ['password_param']
except:
    password_param = '/CsvUploader/password'


def csv_uploader(event, context):
    event_record = event["Records"]
    s3_bucket = event_record[0]["s3"]["bucket"]["name"]
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    
    csv_file_name = 'file.csv'
    final_forward_slash = key.rfind("/")
    if final_forward_slash > -1:
        try:
            file_name_start = final_forward_slash + 1
            csv_file_name = key[file_name_start:]
        except:
            logger.info("Issue getting file name, so using a default file name")

    response = s3.get_object(Bucket=s3_bucket, Key=key)
    csv_file = response["Body"]

    is_valid_csv = validate_csv_file(csv_file)
    
    customer_name = 'temp'
    
    if is_valid_csv:
        username, password = get_credentials()
        auth_token = get_auth_token(auth_token_url, username, password)
        upload_status = upload_csv_file(csv_upload_url, customer_name, auth_token, s3_bucket, key, csv_file_name)
        if upload_status == True:
            logger.info("File with {key} name is uploaded".format(key=key))
        else:
            logger.info("Could not upload file with {key} name".format(key=key))
    else:
        logger.info("File with {key} name is not valid".format(key=key))
    
def get_credentials():
    username = None
    password = None
    response = ssm.get_parameters(Names=[username_param, password_param], WithDecryption=True)
    
    for parameter in response['Parameters']:
        if "username" in parameter['Name']:
            username = parameter['Value']
        else:
            password = parameter['Value']
    
    return username, password

def get_auth_token(url, username, password):
    auth_token = None
    headers = {'Content-Type':'application/json'}
    payload = {"username":username, "password":password}
    
    token_req = requests.post(url, headers=headers, data=json.dumps(payload))                
    
    if token_req.status_code == 200:
        resp = json.loads(token_req.content)
        auth_token = resp['token']
    else:
        raise Exception("An error has occurred while trying to request auth token")        

    return auth_token

def validate_csv_file(csv_file):
    df = pd.read_csv(csv_file)
    headers = df.columns

    headers_lower_case = [header.lower() for header in headers]

    num_of_required_headers = len(required_headers)
    is_all_required_headers_present = False
    required_headers_present = 0
    missing_headers = []

    for header in required_headers:
        if header in headers_lower_case:
            required_headers_present += 1
        else:
            missing_headers.append(header)

    if num_of_required_headers == required_headers_present:
        is_all_required_headers_present = True
        logger.info("All the headers are present")
    else:
        logger.info("The following headers are missing " + str(missing_headers))

    return is_all_required_headers_present


def upload_csv_file(url, customer_name, token, bucket_name, key, csv_file_name):
    upload_headers = {'cust':customer_name, 'Authorization': token}
    response = s3.get_object(Bucket=bucket_name, Key=key)
    csv_file_bytes = io.BytesIO(response["Body"].read())
    files = {'file': (csv_file_name, csv_file_bytes, 'text/csv')}
    
    upload_status = False

    upload_req = requests.post(url, headers=upload_headers, files=files)
    
    if upload_req.status_code == 200:
        upload_status = True
    else:
        resp = json.loads(upload_req.content)
        logger.error("Status code is {code} and response is {response}".format(code=upload_req.status_code, response=resp))
        raise Exception("File upload failed")
        
    return upload_status